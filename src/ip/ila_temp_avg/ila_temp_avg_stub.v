// Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2022.1 (win64) Build 3526262 Mon Apr 18 15:48:16 MDT 2022
// Date        : Sat Apr  1 14:28:42 2023
// Host        : PCPHESE71 running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode synth_stub -rename_top ila_temp_avg -prefix
//               ila_temp_avg_ ila_temp_avg_stub.v
// Design      : ila_temp_avg
// Purpose     : Stub declaration of top-level module interface
// Device      : xczu9eg-ffvb1156-2-e
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* x_core_info = "ila,Vivado 2022.1" *)
module ila_temp_avg(clk, probe0, probe1, probe2, probe3, probe4, probe5, 
  probe6, probe7)
/* synthesis syn_black_box black_box_pad_pin="clk,probe0[0:0],probe1[9:0],probe2[9:0],probe3[17:0],probe4[17:0],probe5[17:0],probe6[23:0],probe7[7:0]" */;
  input clk;
  input [0:0]probe0;
  input [9:0]probe1;
  input [9:0]probe2;
  input [17:0]probe3;
  input [17:0]probe4;
  input [17:0]probe5;
  input [23:0]probe6;
  input [7:0]probe7;
endmodule
